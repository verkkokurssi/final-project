import {useParams} from 'react-router-dom';

const HelloPage = () => {
    let { userName } = useParams();

    return (
        <h1>Hello there, {userName}!</h1>
    )
}

export default HelloPage;