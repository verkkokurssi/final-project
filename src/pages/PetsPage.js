import React from 'react';
import { useState } from 'react';
import {Button, Switch} from '@mui/material';
import { useNavigate } from "react-router-dom";
import toast, { Toaster } from 'react-hot-toast';

const PetsPage = (props) => {

    const [allPets, setAllPets] = useState([]);
    const [pets, setPets] = useState([]);
    const [aliveOnly, setAliveOnly] = useState(false);
    const navigate = useNavigate();

    // If token is empty, redirect to login page
    if(props.token.length === 0) {
        window.location.href = "/login";
        return;
    }

    const toggleAlive = (event) => {
        setAliveOnly(event.target.checked);

        

        if(event.target.checked)
        {
            let filteredPets = [];
            for(let i = 0; i < allPets.length; i++)
            {
                let pet = allPets[i];
                if(pet.status === "alive")
                {
                    filteredPets.push(pet);
                }
            }
            toast("Showing only alive pets!");
            setPets(filteredPets);
        }
        else
        {
            toast("Showing all pets!");
            setPets(allPets);
        }
    }


    // Get data and prevent infinite loop
    if(pets.length === 0)
    {
        fetch('https://test.mrrockis.com:4000/pets', {
            headers: {
                Accept: 'application/json',
                Authorization: 'Bearer ' + props.token
              }
        })
        .then(response => response.json())
        .then(data => {
            
            let petData = data;

            // Get last visits
            fetch('https://test.mrrockis.com:4000/visits', {
                headers: {
                    Accept: 'application/json',
                    Authorization: 'Bearer ' + props.token
                }
            })
            .then(response => response.json())
            .then(data => {
                //console.log(data);

                // Get latest visit for each pet in petData
                for(let i = 0; i < petData.length; i++)
                {
                    let pet = petData[i];
                    let latestDate = null;
                    for(let j = 0; j < data.length; j++)
                    {
                        let visit = data[j];
                        if(visit.petId === pet.id)
                        {
                            if(latestDate === null || latestDate < visit.date)
                            {
                                latestDate = visit.date;
                                pet.visitDate = visit.date;
                            }
                        }
                    }
                }

                // Check if pet does not have a visit date
                for(let i = 0; i < petData.length; i++)
                {
                    let pet = petData[i];
                    if(pet.visitDate === undefined)
                    {
                        pet.visitDate = "Not visited!";
                    }
                }

                setAllPets(petData);
                setPets(petData);
            })
            .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
    }

    return (
        <>
            <div><Toaster/></div>
            <h1>Pets:</h1>

            { /* Add pet button and toggle for showing only alive pets */ }
            <div style={{margin: "auto"}}>
                <Button variant="contained" onClick={
                    () => {
                        navigate("/add-pet");
                    }
                }>Add pet</Button>
              <Switch sx={{my: "auto"}} onChange={toggleAlive} color="warning"/><span>Show only alive</span>
            </div>

            <h2>Pet count: {pets.length}</h2>

            <table>
                <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Owner ID</th>
                        <th>Date of birth</th>
                        <th>Status</th>
                        <th>Last visit</th>
                    </tr>

                    { pets.map((pet) => (
                        <tr onClick={() => navigate("/pet/" + pet.id)} key={pet.id}>
                            <td>{pet.id}</td>
                            <td>{pet.name}</td>
                            <td>{pet.petType}</td>
                            <td>{pet.ownerId}</td>
                            <td>{pet.dob}</td>
                            <td>{pet.status}</td>
                            <td>{pet.visitDate}</td>
                        </tr>
                    ))} 
                </tbody>
            </table>

            <h2>You can only see the pets you have access to.</h2>
        </>
    )
}

export default PetsPage;