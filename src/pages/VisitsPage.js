import React from 'react';
import { useState } from 'react';
import toast, { Toaster } from 'react-hot-toast';

const VisitsPage = (props) => {

    const [visits, setVisits] = useState([]);
    const [visitsLoaded, setVisitsLoaded] = useState(false);

    // If token is empty, redirect to login page
    if(props.token.length === 0) {
        window.location.href = "/login";
        return;
    }

    // Get visits and prevent infinite loop
    if(!visitsLoaded)
    {
       setVisitsLoaded(true);

        fetch('https://test.mrrockis.com:4000/visits', {
            headers: {
                Accept: 'application/json',
                Authorization: 'Bearer ' + props.token
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            
            // Remove visits from the past
            let today = new Date();
            today.setHours(0, 0, 0, 0);
            let filteredVisits = [];
            for(let i = 0; i < data.length; i++)
            {
                let visit = data[i];

                // if visit.comment is empty
                if(!visit.comment)
                {
                    visit.comment = "No comment";
                }

                let visitDate = new Date(visit.date);
                if(visitDate >= today)
                {
                    filteredVisits.push(visit);
                }
            }

            // Sort visits newest to oldest
            filteredVisits.sort((a, b) => {
                return new Date(b.date) - new Date(a.date);
            });

            setVisits(filteredVisits);
            toast.success("Visits loaded!");
        })
        .catch(error => console.error(error));
    }

    return (
        <>
            <div><Toaster/></div>
            <h1>Visits:</h1>
            <h5>Past visits are hidden</h5>
            <ul>
                {visits.length === 0 && <li>No future visits</li>}
                {visits.map((visit) => (
                    <div key={visit.id}>
                    <li key={visit.id}>{visit.date} - Pet: {visit.petId} - Comment: {visit.comment}</li>
                    </div>
                ))}
            </ul>

            <h2>You can only see the visits for your own pets. Doctors can see every visit here.</h2>
        </>
    )
}

export default VisitsPage;