import React from 'react';
import { useParams } from 'react-router-dom';
import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import { Button } from '@mui/material';
import toast, { Toaster } from 'react-hot-toast';


const PetPage = (props) => {
    let { petId } = useParams();

    const [pet, setPet] = useState(null);
    const navigate = useNavigate();

    // If token is empty, redirect to login page
    if(props.token.length === 0) {
        window.location.href = "/login";
        return;
    }

    const addVisit = (event) => {
        event.preventDefault();
    
        const visitDate = event.target.visitDate.value;
        const visitComment = event.target.visitComment.value;
    
        console.log("date: " + visitDate);
        console.log("comment: " + visitComment);

        // Check if date is empty
        if(visitDate.length === 0)
        {
            //alert("Please enter a date to continue!");
            toast.error("Please enter a date to continue!");
            return;
        }

        // If user is not doctor, check for additional validation
        if(props.userId != 0)
        {
            // Check if comment is empty
            if(visitComment.length === 0)
            {
                //alert("Please enter a comment to continue!");
                toast.error("Please enter a comment to continue!");
                return;
            }

            // Check if there is already a visit on this date
            for(let i = 0; i < pet.visits.length; i++)
            {
                let visit = pet.visits[i];
                if(visit.date === visitDate)
                {
                    //alert("There is already a visit on this date! Please choose another date!");
                    toast.error("There is already a visit on this date! Please choose another date!");
                    return;
                }
            }

            // Check that the date is in the future
            if(new Date(visitDate) < new Date())
            {
                //alert("The date must be in the future!");
                toast.error("The date must be in the future!");
                return;
            }
        }
    
        const details = {
            date: visitDate,
            petId: pet.id,
            comment: visitComment
          };
        
        const options = {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + props.token
            },
            body: JSON.stringify(details)
          };
          fetch('https://test.mrrockis.com:4000/visits', options)
            .then(response => response.json())
            .then(data => {
                console.log(data);

                // Add visit to pet
                pet.visits.push(data.visit);

                // Sort visits newest to oldest
                pet.visits.sort((a, b) => {
                    return new Date(b.date) - new Date(a.date);
                });

                // Trigger a re-render
                setPet(null);
                toast.success("Visit created!");
            })
            .catch(error => {
                console.error("Error creating visit: " + error);
                //alert("Error creating visit please try again!");
                toast.error("Error creating visit please try again!");
            });
    }

    const saveNote = (event) => {
        event.preventDefault();
    
        const note = event.target.doctorNote.value;
        localStorage.setItem(doctorNoteId, note);
        toast.success("Note saved!");
    }

    const onStatusUpdate = (event) => {
        const status = event.target.value;
        pet.status = status;
        setPet(null);

        const details = {
            status: status
          };
        
        const options = {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + props.token
            },
            body: JSON.stringify(details)
          };
          fetch('https://test.mrrockis.com:4000/pets/' + petId, options)
            .then(response => response.json())
            .then(data => {
                if(data.message === undefined)
                {
                    //alert("Error changing pet status please try again!");
                    toast.error("Error changing pet status please try again!")
                    return;
                }

                console.log(data);
                toast("Pet status changed to " + status);
            })
            .catch(error => {
                console.error("Error changing pet status: " + error);
                //alert("Error changing pet status please try again!");
                toast.error("Error changing pet status please try again!");
            });
    }

    // Get pet data
    if (!pet) {
        fetch('https://test.mrrockis.com:4000/pets/' + petId, {
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + props.token
          }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);

            // Check if pet was found
            if(data.id === undefined)
            {
                alert("Pet could not be found!");
                navigate("/pets/");
                return;
            }

            let petData = data;

            // Get last visits
            fetch('https://test.mrrockis.com:4000/visits', {
                headers: {
                    Accept: 'application/json',
                    Authorization: 'Bearer ' + props.token
                }
            })
            .then(response => response.json())
            .then(data => {
                // Get visits for pet
                let visits = [];
                for(let i = 0; i < data.length; i++)
                {
                    let visit = data[i];
                    if(visit.petId === petData.id)
                    {
                        visits.push(visit);
                    }
                }

                // Sort visits newest to oldest
                visits.sort((a, b) => {
                    return new Date(b.date) - new Date(a.date);
                });

                petData.visits = visits;

                setPet(petData);
            })
            .catch(error => console.error(error));
        })
        .catch(error => console.error(error));

        return <p>Loading pet details...</p>;
      }

      let doctorNoteId = "doctorNote" + pet.id;

    return (
        <>
            <div><Toaster/></div>
            <h1>Details for pet {pet.id}</h1>

            { /* Back button */ }
            <div style={{margin: "auto"}}>
                <Button variant="contained" onClick={
                    () => {
                        navigate("/pets/");
                    }
                }>Back to pets</Button>
            </div>

            <h2>Name: {pet.name}</h2>
            <h2>Type: {pet.petType}</h2>
            <h2>Owner ID: {pet.ownerId}</h2>
            <h2>Date of birth: {pet.dob}</h2>
            <h2>Status:</h2>

            {props.userId !== 0 ?
                <select disabled onChange={onStatusUpdate} value={pet.status} id="status" name="status">
                    <option value="alive">Alive</option>
                    <option value="deceased">Deceased</option>
                    <option value="missing">Missing</option>
                    <option value="other">Other</option>
                </select>
                :
                <select onChange={onStatusUpdate} value={pet.status} id="status" name="status">
                    <option value="alive">Alive</option>
                    <option value="deceased">Deceased</option>
                    <option value="missing">Missing</option>
                    <option value="other">Other</option>
                </select>
            }

            
            <h2>Visits:</h2>
            <ul>
                {pet.visits.length === 0 && <li>No visits</li>}
                {pet.visits.map((visit) => (
                    <div key={visit.id}>
                    <li key={visit.id}>{visit.date} {visit.comment}</li>
                    </div>
                ))}
            </ul>

            { /* Doctors note */}
            { props.userId == 0 && 
                <div>
                    <h2>Doctors Note</h2>
                    { /* Note not provided by the API ??? Save to local storage instead */}
                    <form onSubmit={saveNote}>
                        <input type="text" id="doctorNote" name="doctorNote" defaultValue={localStorage.getItem(doctorNoteId)}></input>
                        <button type="submit">Save Note</button>
                    </form>
                </div>
            }

            <br></br><br></br>
            <h2>Create Visit</h2>
            

            <form onSubmit={addVisit}>
                <input type="date" id="visitDate" name="visitDate"></input>
                <input type="text" id="visitComment" name="visitComment"></input>
                <button type="submit">Create Visit</button>
            </form>
        </>
    )
}

export default PetPage;