import React from 'react';
import { useNavigate } from "react-router-dom";
import toast, { Toaster } from 'react-hot-toast';

const AddPetPage = (props) => {

    const navigate = useNavigate();

    // If token is empty, redirect to login page
    if(props.token.length === 0) {
        window.location.href = "/login";
        return;
    }

    const addPet = (event) => {
        event.preventDefault();

        // TODO: add client feedback

        const form = event.target;
        const name = form.name.value;
        const type = form.type.value;
        const dob = form.dob.value;

        const details = {
            name: name,
            petType: type,
            dob: dob,
            ownerId: props.userId
          };

        const options = {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + props.token
            },
            body: JSON.stringify(details)
          };

          fetch('https://test.mrrockis.com:4000/pets', options)
            .then(response => response.json())
            .then(data => {
                console.log(data);

                // Check if adding pet was successful
                if(data.pet === undefined)
                {
                    //alert("Could not add pet! Please check your details!");
                    toast.error("Could not add pet! Please check your details!");
                    return;
                }

                navigate("/pet/" + data.pet.id);
            })
            .catch(error => {
                //console.error(error);
                //alert("Error adding a pet in please try again!");
                toast.error("Error adding a pet in please try again!");
            });
    }

    return (
        <>
            <div><Toaster/></div>
            <h1>Add pet</h1>
            <p>You can add a pet with the form below</p>

            <form onSubmit={addPet}>
                    <input type="text" id='name' placeholder='Pet name'/><br></br>
                    <input type="text" id='type' placeholder='Type'/><br></br>
                    <input type="text" id='dob' placeholder='Date of birth'/><br></br>

                    <br></br><button type="submit">Add Pet</button>
            </form>
        </>
    )
}

export default AddPetPage;