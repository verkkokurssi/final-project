import Cookies from 'js-cookie';
import toast, { Toaster } from 'react-hot-toast';
import { Formik, Field, Form } from 'formik';

const LoginPage = (props) => {

    const loggedIn = props.token.length > 0;

    const login = (values) => {
        const email = values.email;
        const password = values.password;

        console.log("email: " + email);
        console.log("password: " + password);

        const details = {
            email: email,
            password: password
          };
        
        const options = {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(details)
          };
          fetch('https://test.mrrockis.com:4000/login', options)
            .then(response => response.json())
            .then(data => {
                // Check if login was successful
                if(data.access_token === undefined)
                {
                    //alert("Invalid username or password!");
                    toast.error("Invalid username or password!")
                    return;
                }

                // Set properties and cache in cookies
                props.setToken(data.access_token);
                props.setUser(email);
                props.setUserId(data.id);

                Cookies.set('token', data.access_token, { expires: 7 });
                Cookies.set('user', email, { expires: 7 });
                Cookies.set('userId', data.id, { expires: 7 });

                toast.success("Logged in as " + email + "!");
            })
            .catch(error => {
                console.error("Error logging in: " + error);
                //alert("Error logging in please try again!");
                toast.error("Error logging in please try again!");
            });
    }

    const logout = (event) => {
        event.preventDefault();
        
        props.setToken("");
        props.setUser("");

        Cookies.remove('token');
        Cookies.remove('user');
        toast.success("Logged out!");
    }

    return (
        <>
            <div><Toaster/></div>
            {loggedIn ? (
              <>
                <h1>Logout</h1>
                <p>User: {props.user} ({props.userId})</p>
                <p>You are logged in!</p>
                <form onSubmit={logout}>
                    <button type="submit">Logout</button>
                </form>
              </>
              ) : (
              <>
                <h1>Login</h1>
                <p>You are not logged in!</p>

                <Formik
                  initialValues={{
                    email: 'doctor@pets.com',
                    password: 'Pet1234',
                  }}
                  onSubmit={login}
                >
                  <Form>
                    <label htmlFor="email">Email:</label>
                    <Field
                      id="email"
                      name="email"
                      placeholder="email@email.com"
                      type="email"
                    />

                    <label htmlFor="password">Password:</label>
                    <Field
                      id="password"
                      name="password"
                      placeholder="password"
                      type="password"
                    />

                    <button type="submit">Login!</button>
                  </Form>
                </Formik>
                
                <br></br>
                <h2>Test accounts</h2>
                <p>Doctor: doctor@pets.com / Pet1234</p>
                <p>Owner 1: owner1@test.com / qwerty</p>
                <p>Owner 2: owner2@woof.net / Bark!</p>
                <p>Owner 3: owner3@abc.org / _Dog2023</p>
              </>
              )
            }
        </>
    )
}

export default LoginPage;