import logo from './logo.svg';
import './App.css';
import AppNavbar from './routing/AppNavbar';
import AppRoutes from './routing/AppRoutes';
import { useState } from 'react';
import Cookies from 'js-cookie';

const App = () => {

  const [token, setToken] = useState(Cookies.get('token') || "");
  const [user, setUser] = useState(Cookies.get('user') || "");
  const [userId, setUserId] = useState(Cookies.get('userId') || "");

  return (
    <>
      <AppNavbar token={token} user={user}/>
      <AppRoutes token={token} setToken={setToken} user={user} userId={userId} setUser={setUser} setUserId={setUserId}/>
    </>
  );
}

export default App;