import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom'
import App from './App';
import {BrowserRouter, MemoryRouter} from 'react-router-dom'

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // Deprecated
    removeListener: jest.fn(), // Deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

test('App rendering/navigating with react router', async () => {
  render(<App />, {wrapper: BrowserRouter})
  const user = userEvent.setup()

  // Check default page content
  expect(screen.getByText(/pet clinic/i)).toBeInTheDocument()

  // Navigate to login page and verify content
  await user.click(screen.getByText(/login/i))
  expect(screen.getByText(/you are not logged in/i)).toBeInTheDocument()
})


test('Login with invalid user', async () => {
  render(<App />, {wrapper: BrowserRouter})

  const user = userEvent.setup()

  // Login with invalid user
  const emailInputNode = screen.getByLabelText("Email:");
  await user.clear(emailInputNode)
  await user.type(emailInputNode, 'fasf@gagwa.com')
  
  const passwordInputNode = screen.getByLabelText("Password:");
  await user.clear(passwordInputNode)
  await user.type(passwordInputNode, 'sagawg')

  await user.click(screen.getByRole('button', {name: /login!/i}))

  await waitFor(() =>
    expect(screen.getByText(/invalid username or password!/i)).toBeInTheDocument(),
  )
})


test('Login with user', async () => {
  render(<App />, {wrapper: BrowserRouter})

  const user = userEvent.setup()

  // Login with user
  const emailInputNode = screen.getByLabelText("Email:");
  await user.clear(emailInputNode)
  await user.type(emailInputNode, 'doctor@pets.com')
  
  const passwordInputNode = screen.getByLabelText("Password:");
  await user.clear(passwordInputNode)
  await user.type(passwordInputNode, 'Pet1234')

  await user.click(screen.getByRole('button', {name: /login!/i}))

  await waitFor(() =>
    expect(screen.getByText(/you are logged in/i)).toBeInTheDocument(),
  )
})


test('List pets', async () => {
  render(<App />, {wrapper: BrowserRouter})

  const user = userEvent.setup()

  // Navigate to pets page
  await user.click(screen.getByText(/list pets/i))

  // Check page for pets
  await waitFor(() => {
    const tableRows = screen.getAllByRole('row');
    expect(tableRows.length).toBeGreaterThan(1);

    // Check default pet data name
    expect(screen.getByText(/buddy/i)).toBeInTheDocument();
  });
})


test('Ensure pet info page works', async () => {
  render(<App />, {wrapper: BrowserRouter})

  const user = userEvent.setup()

  // Navigate to pets page
  await user.click(screen.getByText(/list pets/i))

  // Check page for pets
  await waitFor(() => {
      // Click the default "Buddy" pet
       user.click(screen.getByText(/buddy/i))

      // Wait for page to load and check for pet info
       waitFor(() => {
        expect(screen.getByText(/2019-10-07/i)).toBeInTheDocument();
        expect(screen.getByText(/type: dog/i)).toBeInTheDocument();
      });
  });
})


test('Does visit page work', async () => {
  render(<App />, {wrapper: BrowserRouter})

  const user = userEvent.setup()

  // Navigate to visits page
  await user.click(screen.getByText(/visits/i))

  // Check page for default "no future visits" text
  await waitFor(() => {
    expect(screen.getByText(/no future visits/i)).toBeInTheDocument();
  });
})