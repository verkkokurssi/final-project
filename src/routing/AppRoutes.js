import {Routes, Route} from 'react-router-dom';
import {Container} from '@mui/material';
import HomePage from '../pages/HomePage';
import LoginPage from '../pages/LoginPage';
import PetsPage from '../pages/PetsPage';
import PetPage from '../pages/PetPage';
import VisitsPage from '../pages/VisitsPage';
import AddPetPage from '../pages/AddPetPage';
import NotFoundPage from '../pages/NotFoundPage';

const AppRoutes = (props) => {
    return (
    <Container>
        <Routes>
          <Route path="/" element={<HomePage />}></Route>
          <Route path="/login" element={<LoginPage token={props.token} setToken={props.setToken} user={props.user} setUser={props.setUser} userId={props.userId} setUserId={props.setUserId}/>}></Route>
          <Route path="/pets" element={<PetsPage token={props.token}/>}></Route>
          <Route path="/add-pet" element={<AddPetPage token={props.token} userId={props.userId}/>}></Route>
          <Route path="/pet/:petId" element={<PetPage token={props.token} userId={props.userId}/>}></Route>
          <Route path="/visits" element={<VisitsPage token={props.token}/>}></Route>
          <Route path="*" element={<NotFoundPage />}/>
        </Routes>
    </Container>
    )
}

export default AppRoutes;