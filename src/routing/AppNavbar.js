import {Link} from 'react-router-dom';
import {AppBar, Container, Button, Box} from '@mui/material';

const AppNavbar = (props) => {
    const loggedIn = props.token.length > 0;

    return (
    <AppBar position="static">
        <Container >
          <Box display="flex">
          {loggedIn ? (
              <>
              <Button sx={{ my: 2, mx: 4, color: 'white', display: 'block' }} component={Link} to={"/"}>Home</Button>
              <Button sx={{ my: 2, mx: 4, color: 'white', display: 'block' }} component={Link} to={"/pets"}>List Pets</Button>
              <Button sx={{ my: 2, mx: 4, color: 'white', display: 'block' }} component={Link} to={"/visits"}>Visits</Button>
              <Button sx={{ my: 2, mx: 4, color: 'white', display: 'block' }} component={Link} to={"/login"}>Logout</Button>
            </>
              ) : (
                <>
                <Button sx={{ my: 2, mx: 4, color: 'white', display: 'block' }} component={Link} to={"/"}>Home</Button>
                <Button sx={{ my: 2, mx: 4, color: 'white', display: 'block' }} component={Link} to={"/login"}>Login</Button>
              </>
              )
            }
          </Box>
        </Container>
      </AppBar>
    )
}

export default AppNavbar;